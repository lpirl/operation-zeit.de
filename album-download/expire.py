#!/bin/sh
''':'
exec python3 -OO "$0" "$@"
'''

import sys
from re import compile as re_compile, IGNORECASE
from datetime import datetime, timedelta
from os import listdir, chdir, remove
from os.path import dirname, islink

LOG_FILE = "/var/log/nginx/access.log"
REGEXP = r'^[0-9abcdef.:]{3,39} - - \[([\w\/:+ ]+)\] "GET \/de\.operation-zeit\/download\/([\w]{8})\/ HTTP.+$'
TIMESTAMP_FORMAT = "%d/%b/%Y:%H:%M:%S %z"
VALIDITY_TIMEOUT = timedelta(hours=1)

# 10.10.1.100 - - [22/Jun/2016:18:03:32 +0000] "GET /de.operation-zeit/download/wgcVM5fJ4AvRO5eveaABnXjF4E6tkXTI/ HTTP/1.0" 200 3487 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36"

chdir(dirname(sys.argv[0]) or '.')
chdir('..')
exisiting_links = set(p for p in listdir(".") if islink(p))

do_match = re_compile(REGEXP, IGNORECASE).match
with open(LOG_FILE, "r") as log_filep:
  for line in log_filep:
    line = line.strip()
    match = do_match(line)
    if match:
      link_name = match.group(2)
      if link_name not in exisiting_links:
        continue
      first_access = datetime.strptime(match.group(1), TIMESTAMP_FORMAT)
      expiration = first_access + VALIDITY_TIMEOUT
      now = datetime.now(tz=first_access.tzinfo)
      if expiration <= now:
        print("disabling:", link_name)
        remove(link_name)
        exisiting_links.remove(link_name)
