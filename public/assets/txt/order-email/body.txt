Hallo Operation Zeit,

hiermit bestelle ich euer Album "Von Füchsen und Coyoten".

Ich zahle dafür einen Betrag von mindestens 12 €
(10 € für die CD + 2 € für Verpackung und Versand).

Wenn ich euch supporten will, zahle ich sogar freiwillig etwas mehr,
weil ich weiß wie teuer so eine Albumproduktion ist. :)

Das Geld überweise ich innerhalb von 10 Werktagen an:
  Lukas Pirl
  IBAN: DE33 2004 1133 0634 0335 40

Das Album schickt ihr mir nach Geldeingang an:
  ********** DEINE POSTANSCHRIFT **********

Ich freue mich auf eure Mucke!

Viele Grüße,

********** DEIN NAME **********
